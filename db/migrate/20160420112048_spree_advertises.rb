class SpreeAdvertises < ActiveRecord::Migration
  def change
  	create_table :spree_advertises do |t|
      t.text :header
      t.text :description
      t.attachment :image 
    end
  end
end
