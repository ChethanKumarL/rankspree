class CreateImages < ActiveRecord::Migration
  def change
    create_table :images do |t|
      t.text :name
      t.attachment :image
      t.text :description
      t.string :image_file_name 
	  t.integer :image_file_size 
	  t.string :image_content_type 
	  t.datetime :image_updated_at 
      t.timestamps null: false
    end
  end
end
