module Spree
  Spree::OrdersController.class_eval do
     def cart
       order = current_order()
       render :json => {'message' => 'cart', 'cart' => create_cart(order.line_items)}
     end
  end
end