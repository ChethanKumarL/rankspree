module Spree
class AdvertisersController < Spree::BaseController
before_action :set_advertiser, only: [ :edit, :update, :destroy]
skip_before_action :authenticate_user

  # GET /advertises
  # GET /advertises.json
  def index
    @advertisers = Spree::Advertise.all
  end

  # GET /advertises/1
  # GET /advertises/1.json
  def show
    @advertiser = Spree::Advertise.find(params[:id])
  end

  # GET /advertises/new
  def new
    @advertiser = Spree::Advertise.new
  end

  # GET /advertises/1/edit
  def edit
    @advertiser = Spree::Advertise.find(params[:id])
  end

  # POST /advertises
  # POST /advertises.json
  def create
    @advertiser = Spree::Advertise.new(advertiser_params)
    respond_to do |format|
      if @advertiser.save
        format.html { redirect_to advertisers_url, notice: 'Advertiser was successfully created.' }
        format.json { render :show, status: :created, location: advertisers_url }
      else
        format.html { render :new }
        format.json { render json: @advertiser.errors, status: :unprocessable_entity }
      end
    end
  end
  # PATCH/PUT /advertises/1
  # PATCH/PUT /advertises/1.json
  def update
    respond_to do |format|
      if @advertiser.update(advertiser_params)
        format.html { redirect_to advertisers_url, notice: 'Advertise was successfully updated.' }
        format.json { render :show, status: :ok, location: advertisers_url }
      else
        format.html { render :edit }
        format.json { render json: @advertiser.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /advertises/1
  # DELETE /advertises/1.json
  def advertise_delete
  @advertiser = Spree::Advertise.find(params[:id])    
    @advertiser.delete
    redirect_to :action => 'index'
  end

  # def delete
  #   @advertisers = Spree::Advertise.find(params[:id])
  #   @advertisers.destroy()
  #   redirect_to :action => 'index'
  # end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_advertiser
      @advertiser = Spree::Advertise.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def advertiser_params
      params.require(:advertise).permit(:header, :description, :image)
    end

  end

end

