module Spree
  Spree::OrdersController.class_eval do
     def cart
        unless @order.completed?
          @order.refresh_shipment_rates
        end
        if @order.shipments.shipped.count > 0
          redirect_to edit_admin_order_url(@order)
        end
      end
  end
end