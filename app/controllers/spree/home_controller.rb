module Spree
  class HomeController < Spree::StoreController
    helper 'spree/products'
    respond_to :html

    
    def index
		   @taxons = Spree::Taxon.friendly
           return unless @taxons
           @advts = Spree::Advertise.all
    end
    
  end
end