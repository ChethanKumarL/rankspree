Deface::Override.new(:virtual_path => 'spree/shared/_main_nav_bar',
  :replace => "li#home-link",
  :text => "<li id='home-link' data-hook><%= link_to 'Home', spree.root_path %></li>",
  :name => "change_home_text")
