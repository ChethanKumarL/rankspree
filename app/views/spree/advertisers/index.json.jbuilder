json.array!(@advertises) do |advertise|
  json.extract! advertise, :id
  json.url advertise_url(advertise, format: :json)
end
